import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Catalog from './components/catalog';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Catalog} />
        </Switch>
      </Router>
    );
  }
}

export default App;
