import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Styles = styled.div`
  .product-item {
    display: flex;
    flex-flow: row;
    align-items: center;
    border-top: 1px solid rgb(237, 234, 233);
    border-left: 1px solid rgb(237, 234, 233);
    border-right: 1px solid rgb(237, 234, 233);
    padding: 0.5vh 4vh;
  }
  .product-item .product-images {
    display: flex;
    align-items: center;
  }
  .product-item .product-images img {
    width: 13vh;
    margin-right: 1vh;
  }
  .product-item .product-detail {
    display: flex;
    flex-flow: row;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    padding-left: 2vh;
  }
  .product-item .product-detail .product-info {
    display: flex;
    flex-flow: column;
  }
  .product-item .product-info .product-name {
    font-size: 2.1vh;
    font-weight: 500;
    color: rgba(0, 0, 0, 0.7);
  }
  .product-item .product-info .product-description {
    font-size: 1.6vh;
    font-weight: 400;
    color: rgba(0, 0, 0, 0.5);
    padding-top: 0.5vh;
  }
  .product-price {
    font-size: 2.1vh;
    font-weight: 400;
    color: rgba(0, 0, 0, 0.5);
  }
  .product-price .original-price {
    text-decoration: line-through;
  }
  .product-price .final-price {
    font-size: 2.1vh;
    font-weight: 500;
    color: rgba(0, 0, 0, 0.7);
  }
  @media only screen and (max-width: 1024px) {
    .display-none {
      display: none
    }
  }
  @media only screen and (max-width: 799px) {
    .product-item {
      display: flex;
      flex-flow: row;
      align-items: center;
      border-top: 1px solid rgb(237, 234, 233);
      border-left: 1px solid rgb(237, 234, 233);
      border-right: 1px solid rgb(237, 234, 233);
      padding: 0.5vh 2vh;
    }
    .product-item .product-images {
      display: flex;
      align-items: center;
    }
    .product-item .product-images img {
      width: 10vh;
      margin-right: 0;
    }
    .product-item .product-detail {
      display: flex;
      flex-flow: row;
      align-items: center;
      justify-content: space-between;
      width: 100%;
      padding-left: 2vh;
    }
    .product-item .product-detail .product-info {
      display: flex;
      flex-flow: column;
    }
    .product-item .product-info .product-name {
      font-size: 1.8vh;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.7);
    }
    .product-item .product-info .product-description {
      font-size: 1.4vh;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.5);
      padding-top: 0.5vh;
    }
    .product-price {
      font-size: 1.6vh;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.5);
    }
    .product-price .original-price {
      text-decoration: line-through;
      font-size: 1.6vh;
      font-weight: 400;
    }
    .product-price .final-price {
      font-size: 1.6vh;
      font-weight: 400;
      color: rgba(0, 0, 0, 0.7);
    }
    .display-none {
      display: none
    }
  }
`;

interface IProps {
  key: number,
  product: ProductObj
}

interface ProductObj {
  id: number,
  name: string,
  description: string,
  productType: string,
  imageSrc1: string,
  imageSrc2: string,
  imageSrc3: string,
  imageSrc4: string,
  discount: number,
  price: number
}

export default class ProductListItem extends Component<IProps, {}> {

  render() {
    const { product } = this.props;
    const descriptionArr = product.description.split(", ");
    const finalPrice = product.price*(1-product.discount/100);

    return (
      <Styles>
        <div className="product-item">
          <div className="product-images">
            <img src={product.imageSrc1} alt={"img1"} />
            <img className="display-none" src={product.imageSrc2} alt={"img2"} />
            <img className="display-none" src={product.imageSrc3} alt={"img3"} />
            <img className="display-none" src={product.imageSrc4} alt={"img4"} />
          </div>
          <div className="product-detail">
            <div className="product-info">
              <div className="product-name">
                {product.name}
              </div>
              <div className="product-description">
                {descriptionArr[0]}{' \u00b7 '}{descriptionArr[1]}
              </div>
            </div>
            <div className="product-price">
              <span className="original-price">{product.price.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})}</span>
              <span> por </span>
              <span className="final-price">{finalPrice.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})}</span>
            </div>
          </div>
        </div>
      </Styles>
    );
  }
}
