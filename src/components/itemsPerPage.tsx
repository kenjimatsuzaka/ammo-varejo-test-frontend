import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Styles = styled.div`
  .items-per-page {
    border: 1px solid rgb(204, 204, 204);
    border-radius: 4px;
    background-color: rgb(255, 255, 255);
    padding: 0.5vh 0.5vh;
    color: rgb(142, 142, 142);
    font-size: 1.5vh;
    outline: none;
  }
  .items-per-page option:hover {
    background-color: rgb(204, 204, 204);
    color: rgb(255, 255, 255);
  }
  .form-group {
    display: flex;
  }
  @media only screen and (max-width: 799px) {
    .form-group {
      display: flex;
      width: 23vh;
      padding-top: 2vh;
    }
    .items-per-page {
      border: 0.1px solid rgb(204, 204, 204);
      border-radius: 4px;
      background-color: rgb(255, 255, 255);
      padding: 0.4vh 0.5vh;
      color: rgb(142, 142, 142);
      font-size: 1.8vh;
      outline: none;
    }
    .items-per-page option:hover {
      background-color: rgb(204, 204, 204);
      color: rgb(255, 255, 255);
    }
  }
`;

interface IProps {
  itemsPerPage: number,
  onChange: React.ReactNode
};

interface IState {
  itemsPerPage: number
}

export default class ItemsPerPage extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      itemsPerPage: this.props.itemsPerPage
    }

    this.handleChange = this.handleChange.bind(this);
  }

  private handleChange(e: React.FormEvent<HTMLSelectElement>): void {
    const value: number = Number(e.currentTarget.value);
    const change: any = this.props.onChange;
    const page: number = 1;

    this.setState({ itemsPerPage: value });
    change(value, page);
  }

  render() {
    const { itemsPerPage } = this.props;

    return (
      <Styles>
      <div className="form-group">
        <select className="items-per-page" value={itemsPerPage} onChange={this.handleChange}>
          <option value="16">16 produtos por página</option>
          <option value="24">24 produtos por página</option>
          <option value="32">32 produtos por página</option>
        </select>
      </div>
      </Styles>
    );
  }
}
