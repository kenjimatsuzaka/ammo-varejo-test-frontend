import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import search from '../images/icon-search.png';
import reset from '../images/icon-x.png';

const Styles = styled.div`
  .search-product {
    padding: 2px;
    width: 50vh;
  }
  .search-form {
    display: flex;
    flex-flow: column;
    border-radius: 25px;
    border: 1px solid rgb(237, 234, 233);
    width: auto;
    height: 2vh;
    padding: 0.7vh 0.7vh;
  }
  .label {
    display: flex;
    width: auto;
  }
  .icon {
    display: flex;
    width: 2vh;
    padding-left: 1.5vh;
    padding-right: 1.5vh;
  }
  .form-control {
    display: flex;
    border: none;
    width: 60vh;
    outline: none;
    font-size: 1.5vh;
    color: rgb(142, 142, 142);
  }
  .reset-btn {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
    padding-right: 2vh;
  }
  .reset-btn img {
    width: 1.8vh;
  }
  .reset-btn-none {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
    padding-right: 2vh;
    display: none;
  }
  @media only screen and (max-width: 799px) {
    .search-product {
      padding: 2px;
      width: 40vh;
    }
    .search-form {
      display: flex;
      flex-flow: column;
      border-radius: 25px;
      border: 1px solid rgb(237, 234, 233);
      width: auto;
      height: 2vh;
      padding: 0.7vh 0.7vh;
    }
    .label {
      display: flex;
      width: auto;
    }
    .icon {
      display: flex;
      width: 2vh;
      padding-left: 1.5vh;
      padding-right: 1.5vh;
    }
    .form-control {
      display: flex;
      border: none;
      width: 80vh;
      outline: none;
      font-size: 1.5vh;
      color: rgb(142, 142, 142);
    }
    .reset-btn {
      display: flex;
      background-color: Transparent;
      background-repeat:no-repeat;
      border: none;
      cursor:pointer;
      overflow: hidden;
      outline:none;
      padding-right: 2vh;
    }
    .reset-btn img {
      width: 1.8vh;
    }
    .reset-btn-none {
      background-color: Transparent;
      background-repeat:no-repeat;
      border: none;
      cursor:pointer;
      overflow: hidden;
      outline:none;
      padding-right: 2vh;
      display: none;
    }
  }
`;

interface IProps {
  onSubmit: React.ReactNode
}

interface IState {
  searchProduct: string
}

export default class SearchProduct extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      searchProduct: ''
    };

    this.submitSearchProduct = this.submitSearchProduct.bind(this);
    this.inputUpdated = this.inputUpdated.bind(this);
    this.resetInput = this.resetInput.bind(this);
  }

  private submitSearchProduct(e: React.FormEvent<HTMLFormElement>): void {
    e.preventDefault();

    const { searchProduct } = this.state;
    const onSubmit: any = this.props.onSubmit;
    const itemsPerPage = 16;
    const page = 1;

    onSubmit(searchProduct, itemsPerPage, page);
  }

  private inputUpdated(e: React.FormEvent<HTMLInputElement>): void {
    const value = String(e.currentTarget.value);

    this.setState({ searchProduct: value });
  }

  private resetInput(e: React.FormEvent<HTMLButtonElement>): void {

    this.setState({ searchProduct: '' });
  }

  render() {
    const { searchProduct } = this.state;

    let resetButton = <button className="reset-btn" onClick={this.resetInput}><img src={reset} alt={"reset"} /></button>;
    if (searchProduct === '') {
      resetButton = <button className="reset-btn-none" onClick={this.resetInput}><img src={reset} alt={"reset"} /></button>;
    }

    return(
      <Styles>
      <div className="search-product">
        <form className="search-form" onSubmit={this.submitSearchProduct}>
          <label className="label" htmlFor="searchProduct">
            <img src={search} className="icon" alt={"search"}/>
            <input className="form-control" type="input" name="searchProduct" value={searchProduct} onChange={this.inputUpdated} />
            <input type="submit" hidden/>
            {resetButton}
          </label>
        </form>
      </div>
      </Styles>
    );
  }
}
