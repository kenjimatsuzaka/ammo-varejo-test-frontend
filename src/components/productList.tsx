import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ProductListItem from './productListItem';

const Styles = styled.div`
  .product-list {
    padding-top: 2vh;
    border-bottom: 1px solid rgb(237, 234, 233);
  }
`;

interface IProps {
  count: number,
  products: Array<string>,
  itemsPerPage: number,
  page: number
}

export default class ProductList extends Component<IProps, {}> {

  render() {
    const { count, products, itemsPerPage, page } = this.props;

    const startIndex = itemsPerPage * (page-1);
    const endIndex = itemsPerPage * page;
    const productsToList = products.slice(startIndex, endIndex);

    return (
      <Styles>
        <div className="product-list">
        {
          count > 0 && productsToList.map((product) => {
            let productObj: any = product;
            return <ProductListItem key={productObj.id} product={productObj} />
          })
        }
        </div>
      </Styles>
    );
  }
}
