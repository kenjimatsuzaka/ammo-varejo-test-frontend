import React, { Component } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import SearchProduct from './searchProduct';
import ProductList from './productList';
import ItemsPerPage from './itemsPerPage';
import Pagination from 'react-js-pagination';
import config from '../config/config.json';

const Styles = styled.div`
  .header {
    display: flex;
    justify-content: space-between;
    padding: 1vh 3vh;
    border-bottom: 0.3vh solid rgb(237, 234, 233);
  }
  .header-logo {
    display: flex;
    width: 14.5vh;
    fill: black;
  }
  .title {
    display: flex;
    align-items: center;
    height: 8vh;
    font-size: 4vh;
    font-weight: 200;
    color: rgb(100, 99, 125);
    background-color: rgb(232, 229, 234);
    border-bottom: 0.3vh solid rgb(237, 234, 233);
  }
  .title div {
    padding: 2.5vh
  }
  .list {
    padding: 3vh 15.5vh;
  }
  .found-products-container {
    height: 5vh;
  }
  .found-products-container div {
    display: flex;
    height: 2.5vh;
    width: 24vh;
    color: rgb(142, 142, 142);
    font-size: 1.55vh;
    border-bottom: 0.3vh solid rgba(216, 152, 86, 0.71);
  }
  .bottom-page {
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-flow: row;
    height: 10vh;
    border-top: 2px solid rgb(237, 234, 233);
    margin-top: 5vh;
  }
  .pagination {
    display: inline-block;
    padding-left: 0;
    margin: 1vh 0;
    border-radius: 4px;
    font-size: 1.2vh;
  }
  .pagination li {
    display: inline;
  }
  .pagination li:first-child a, .pagination li:first-child span {
    margin-left: 0;
  }
  .pagination .disabled a, .pagination .disabled a:focus, .pagination .disabled a:hover, .pagination .disabled>span, .pagination .disabled span:focus, .pagination .disabled span:hover {
    color: rgb(142, 142, 142);
    background-color: rgb(255, 255, 255);
    cursor: not-allowed;
  }
  .pagination .active a, .pagination .active a:focus, .pagination .active a:hover, .pagination .active span, .pagination .active span:focus, .pagination .active span:hover {
    z-index: 3;
    border: 1px solid rgb(204, 204, 204);
    border-radius: 4px;
    cursor: default;
  }
  .pagination li a, .pagination li span {
    position: relative;
    float: left;
    padding: 0.7vh 1.5vh;
    line-height: 1.42857143;
    text-decoration: none;
    color: rgb(142, 142, 142);
    background-color: rgb(255, 255, 255);
    font-size: 1.5vh;
    font-weight: 200;
  }
  @media only screen and (max-width: 799px) {
    .header {
      display: flex;
      justify-content: space-between;
      padding: 0.5vh 1vh;
      border-bottom: 0.1vh solid rgb(237, 234, 233);
    }
    .header-logo {
      display: flex;
      width: 13vh;
      fill: black;
    }
    .title {
      display: flex;
      align-items: center;
      height: 5vh;
      font-size: 3vh;
      font-weight: 200;
      color: rgb(100, 99, 125);
      background-color: rgb(232, 229, 234);
      border-bottom: 0.3vh solid rgb(237, 234, 233);
    }
    .title div {
      padding: 2.5vh
    }
    .list {
      padding: 2vh 2.6vh;
    }
    .found-products-container {
      height: 2.5vh;
    }
    .found-products-container div {
      display: flex;
      height: 2vh;
      width: 20.5vh;
      color: rgb(142, 142, 142);
      font-size: 1.3vh;
      border-bottom: 0.1vh solid rgba(216, 152, 86, 0.71);
    }
    .bottom-page {
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-flow: column;
      height: 6vh;
      border-top: 0.1px solid rgb(237, 234, 233);
      margin-top: 2vh;
    }
    .pagination {
      display: inline-block;
      padding-left: 0;
      margin: 1vh 0;
      border-radius: 4px;
      font-size: 1.2vh;
    }
    .pagination li {
      display: inline;
    }
    .pagination li:first-child a, .pagination li:first-child span {
      margin-left: 0;
    }
    .pagination .disabled a, .pagination .disabled a:focus, .pagination .disabled a:hover, .pagination .disabled>span, .pagination .disabled span:focus, .pagination .disabled span:hover {
      color: rgb(142, 142, 142);
      background-color: rgb(255, 255, 255);
      cursor: not-allowed;
    }
    .pagination .active a, .pagination .active a:focus, .pagination .active a:hover, .pagination .active span, .pagination .active span:focus, .pagination .active span:hover {
      z-index: 3;
      border: 0.1px solid rgb(204, 204, 204);
      border-radius: 4px;
      cursor: default;
    }
    .pagination li a, .pagination li span {
      position: relative;
      float: left;
      padding: 0.7vh 1vh;
      line-height: 1;
      text-decoration: none;
      color: rgb(142, 142, 142);
      background-color: rgb(255, 255, 255);
      font-size: 1.8vh;
      font-weight: 200;
    }
  }
`;

export default class Catalog extends Component<{}, {
  count: number,
  products: Array<string>,
  searchProduct: string,
  page: number,
  itemsPerPage: number
}> {
  constructor(props: {}) {
    super(props);

    this.state = {
      count: 0,
      products: [],
      searchProduct: '',
      page: 1,
      itemsPerPage: 16
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChangeItemsPerPage = this.onChangeItemsPerPage.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentWillMount():void {
    const configObj: any = config;
    const { apiUrl } = configObj[process.env.NODE_ENV];

    axios.get(`${apiUrl}products`)
      .then( ({ data } ) => {
        const count = data.count;
        const products = data.rows;

        this.setState({ count, products });
      })
  }

  private onFormSubmit(searchProduct: string, itemsPerPage: number, page: number): void {
    const configObj: any = config;
    const { apiUrl } = configObj[process.env.NODE_ENV];

    axios.get(`${apiUrl}products?q=${searchProduct}`)
      .then( ({ data } ) => {
        const count = data.count;
        const products = data.rows;

        this.setState({ searchProduct, count, products, itemsPerPage, page });
      })
  }

  private onChangeItemsPerPage(itemsPerPage: number, page: number): void {
    this.setState({ itemsPerPage, page });
  }

  private handlePageChange(pageNumber: number): void {
    this.setState({ page: pageNumber });
  }

  render() {
    const { searchProduct, products, page, itemsPerPage, count } = this.state;

    let title = 'Lista de Produtos';
    if (searchProduct) {
      title = searchProduct;
    }

    return (
      <Styles>
        <div className="header">
          <div className="header-logo">
            <svg id="icon-logo-mmartan" viewBox="0 0 264 32" width="100%" height="100%">
              <title>logo-mmartan</title>
              <path d="M0.188 12.8c0-1.882 0-4.329-0.188-6.212h6.965c0 1.318 0.188 2.635 0.188 3.953 2.824-3.388 5.835-4.894 9.788-4.894 4.518 0 8.282 1.882 10.165 5.647 2.635-4.329 6.588-5.647 10.541-5.647 4.518 0 11.294 1.882 11.294 11.482v14.871h-7.153v-13.365c0-6.212-2.635-7.718-6.212-7.718-3.765 0-7.341 2.824-7.341 9.035v12.047h-7.153v-13.365c0-6.212-2.635-7.718-6.212-7.718-3.765 0-7.341 2.824-7.341 9.035v12.047h-7.341v-19.2z"></path>
              <path d="M58.541 12.8c0-1.882-0.188-4.329-0.188-6.212h6.965c0.188 1.318 0.188 2.635 0.188 3.953 2.824-3.388 5.835-4.894 9.788-4.894 4.518 0 8.282 1.882 10.165 5.647 2.635-4.329 6.588-5.647 10.541-5.647 4.518 0 11.294 1.882 11.294 11.482v14.871h-7.153v-13.365c0-6.212-2.635-7.718-6.212-7.718-3.765 0-7.341 2.824-7.341 9.035v12.047h-7.153v-13.365c0-6.212-2.635-7.718-6.212-7.718-3.765 0-7.341 2.824-7.341 9.035v12.047h-7.153v-19.2z"></path>
              <path d="M142.871 26.353c0 1.694 0 3.012 0.188 4.706h-6.776c-0.188-1.129-0.188-2.259-0.188-3.388-3.012 3.012-6.776 4.329-11.671 4.329-8.659 0-11.482-3.953-11.482-7.529 0-7.718 7.341-9.035 16.565-9.035h6.024c0-4.329-3.012-5.459-6.776-5.459-3.576 0-5.647 1.506-6.024 3.388h-7.718c0.753-6.212 7.529-7.718 13.741-7.718 6.776 0 13.929 1.882 13.929 10.541v10.165zM129.318 19.953c-5.647 0-8.847 1.129-8.847 4.141 0 1.694 1.318 3.953 5.647 3.953 3.576 0 9.224-2.071 9.224-8.094h-6.024z"></path>
              <path d="M150.776 12.235c0-2.259-0.188-4.329-0.188-6.024h7.341c0 1.318 0.188 2.824 0.188 4.329v0c0.753-1.694 2.447-4.894 8.282-4.894 1.694 0 3.012 0.188 4.894 0.565v5.459c-0.376-0.188-0.941-0.188-1.506-0.188s-1.129-0.188-1.882-0.188c-6.212 0-9.412 2.447-9.412 9.035v9.788h-7.529v-17.882z"></path>
              <path d="M185.224 0v6.965h8.659v5.647h-8.659v10.541c0 2.635 1.129 4.141 3.953 4.141 1.506 0 3.576-0.188 4.518-0.376v4.706c-1.882 0.188-4.141 0.376-6.212 0.376-7.341 0-9.6-2.447-9.6-8.094v-11.294h-4.706v-5.647h4.894v-5.271l7.153-1.694z"></path>
              <path d="M227.576 26.353c0 1.694 0 3.012 0.188 4.706h-6.776c-0.188-1.129-0.188-2.259-0.188-3.388-3.012 3.012-6.776 4.329-11.671 4.329-8.471 0-11.482-3.953-11.482-7.529 0-7.718 7.341-9.035 16.565-9.035h5.835c0-4.329-3.012-5.459-6.776-5.459-3.576 0-5.647 1.506-6.024 3.388h-7.718c0.753-6.212 7.529-7.718 13.741-7.718 6.776 0 13.929 1.882 13.929 10.541v10.165zM214.024 19.953c-5.647 0-8.847 1.129-8.847 4.141 0 1.694 1.318 3.953 5.647 3.953 3.576 0 9.224-2.071 9.224-8.094h-6.024z"></path>
              <path d="M235.482 12.612c0-2.259 0-4.329-0.188-6.024h6.965c0.188 1.506 0.188 2.824 0.188 4.141v0c2.447-3.576 5.647-5.082 10.165-5.082 4.329 0 10.918 2.447 10.918 10.729v15.624h-7.153v-13.365c0-6.212-2.635-7.718-6.212-7.718-3.765 0-7.341 2.824-7.341 9.035v12.047h-7.153v-19.388z"></path>
            </svg>
          </div>
          <SearchProduct onSubmit={this.onFormSubmit} />
        </div>
        <div>
          <div className="title">
            <div>{title}</div>
          </div>
          <div className="list">
            <div className="found-products-container">
              <div>{count} PRODUTOS ENCONTRADOS</div>
            </div>
            {
              count > 0 &&
              <ProductList count={count} products={products} itemsPerPage={itemsPerPage*1} page={page} />
            }
            {
              count > 0 &&
              <div className="bottom-page">
                <ItemsPerPage onChange={this.onChangeItemsPerPage} itemsPerPage={itemsPerPage*1} />
                <Pagination
                  activePage={page}
                  itemsCountPerPage={itemsPerPage}
                  totalItemsCount={count}
                  pageRangeDisplayed={5}
                  onChange={this.handlePageChange}
                />
              </div>
            }
          </div>
        </div>
      </Styles>
    );
  }
}
